package com.sumerge;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static int ListLen(List s) throws Exception {
        if(s.size() >= 2)
            return s.size();
        throw new Exception("Employees having that title are less than two");
    }


    public static void main(String[] args) {
	// write your code here
        List<Employee> employees = Arrays.asList(
                new Employee("SE" , "SAM" , "XXXXXXX"),
                new Employee("SE" , "Nancy" , "YYYYYY"),
                new Employee("ASE" , "Mark" , "ZZZZZZ")
        );

        List<String> titles = employees.stream().map(e -> e.getTitle()).distinct().collect(Collectors.toList());

        titles.stream().forEach(title -> {
            List<Employee> s = employees.stream().filter(e -> e.getTitle() == title).collect(Collectors.toList());
            try {
                System.out.print("title :" + title);
                System.out.println(" count :" + ListLen(s));
                s.forEach(e -> System.out.println("name :" + e.getName() + "- mobile :" + e.getMobile()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
